import React from "react";
import styles from "./Main.module.css";
import Achievements from "../Achievements/Achievements";
import AchievementsSeparator from "../../Separators/AchievementsSeparator";
import PreviewGames from "../PreviewsGames/PreviewGames";
import Promotions from "../Promotion/Promotions";
import AllGames from "../AllGames/AllGames";
import Button from "../../Buttons/Button";
import MainButton from "../MainButtons/MainButton";

const Main = props => {
  return (
    <main className={styles.Main}>
      <Achievements winners={props.winners} />
      <AchievementsSeparator />
      <div className={styles.Buttons}>
        <MainButton className={styles.Popular}>popular</MainButton>
        <MainButton className={styles.New}>new</MainButton>
        <MainButton className={styles.Slot}>slot games</MainButton>
        <MainButton className={styles.Card}>card games</MainButton>
        <MainButton className={styles.Roulette}>roulette</MainButton>
      </div>
      <div className={styles.Searching}>
        <div className={styles.Sort}>
          <span className={styles.Sorting}>sort by:</span>
          <button type="button" className={styles.SortProperty}>
            popularity
          </button>
          <div className={styles.SortSeparator} />
          <button type="button" className={styles.SortProperty}>
            a-z
          </button>
        </div>
        <form className={styles.SearchForm}>
          <input
            type="text"
            className={styles.Input}
            placeholder="Search for game..."
            name="search"
          />
        </form>
      </div>
      <section className={styles.MainTop}>
        <PreviewGames previewGames={props.previewGames} />
        <Promotions />
      </section>
      <section className={styles.MainBottom}>
        <AllGames allGames={props.allGames} />
      </section>
      <Button color="blue" className={styles.MoreGames}>
        more games
      </Button>
    </main>
  );
};

export default Main;
