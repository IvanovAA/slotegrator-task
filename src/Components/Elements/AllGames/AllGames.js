import React from "react";
import styles from "./AllGames.module.css";

import Game from "../Game/Game";

const AllGames = props => {
  return (
    <div className={styles.AllGames}>
      {props.allGames.map(item => {
        return (
          <Game
            className={styles.GameItem}
            key={item.key}
            url={item.url}
            title={item.title}
          />
        );
      })}
    </div>
  );
};

export default AllGames;
