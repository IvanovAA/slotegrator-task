import React from "react";
import styles from "./MainButton.module.css";

const MainButton = props => {
  const ImportedStyle = props.className;
  const ButtonStyle = ImportedStyle + " " + styles.MainButton;
  return (
    <button type="button" className={ButtonStyle}>
      {props.children}
    </button>
  );
};

export default MainButton;
