import React from "react";
import styles from "./LastWinners.module.css";

const LastWinners = props => {
  return (
    <div className={styles.LastWinners}>
      <h4 className={styles.Header}>last winners</h4>
      <div className={styles.Container}>
        <button className={styles.ArrowLeft} />
        <ul className={styles.WinnersList}>
          {props.winners.map(item => {
            return (
              <li className={styles.WinnersItem} key={item.id}>
                <div className={styles.Item}>
                  <img
                    className={styles.Logo}
                    src={item.url}
                    alt={item.title}
                    width="69"
                    height="40"
                    title={item.title}
                  />
                  <div className={styles.Description}>
                    <span className={styles.Title} title={item.title}>
                      {item.title}
                    </span>
                    <span className={styles.Score}>{item.cash}$</span>
                    <span className={styles.Player}>{item.playerName}</span>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
        <button className={styles.ArrowRight} />
      </div>
    </div>
  );
};

export default LastWinners;
