import React from "react";
import styles from "./Jackpot.module.css";
import goldMedal from "../../../../assets/images/gold-medal.png";
import goldScore from "../../../../assets/images/golden-score.png";
import silverMedal from "../../../../assets/images/silver-medal.png";
import silverScore from "../../../../assets/images/silver-score.png";
import bronzeMedal from "../../../../assets/images/bronze-medal.png";
import bronzeScore from "../../../../assets/images/bronze-score.png";

const Jackpot = () => {
  return (
    <div className={styles.Jackpot}>
      <h4 className={styles.Header}>jackpot</h4>
      <div className={styles.JackpotContainer}>
        <div className={styles.Item}>
          <img src={goldMedal} className={styles.Medal} alt="gold" />
          <img src={goldScore} className={styles.Score} alt="golden score" />
        </div>
        <div className={styles.Item}>
          <img src={silverMedal} className={styles.Medal} alt="silver" />
          <img src={silverScore} className={styles.Score} alt="silver score" />
        </div>
        <div className={styles.Item}>
          <img src={bronzeMedal} className={styles.Medal} alt="bronze" />
          <img src={bronzeScore} className={styles.Score} alt="bronze score" />
        </div>
      </div>
    </div>
  );
};

export default Jackpot;
