import React from "react";
import styles from "./Acievements.module.css";

import LastWinners from "./LastWinners/LastWinners";
import Jackpot from "./Jackpot/Jackpot";

const Achievements = props => {
  return (
    <section className={styles.Achievements}>
      <div className={styles.Container}>
        <LastWinners winners={props.winners} />
        <Jackpot />
      </div>
    </section>
  );
};

export default Achievements;
