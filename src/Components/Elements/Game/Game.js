import React from "react";
import styles from "./Game.module.css";

import Button from "../../Buttons/Button";

const Game = props => {
  const ImportedStyle = props.className;
  const GameStyle = ImportedStyle + " " + styles.Game;
  return (
    <div className={GameStyle}>
      <div className={styles.Layer}>
        <img src={props.url} alt={props.title} />
        <div className={styles.LayerButtons}>
          <Button color="blue" className={styles.Play}>
            play now
          </Button>
          <Button color="red" className={styles.Demo}>
            demo
          </Button>
        </div>
      </div>
      <span className={styles.Title}>{props.title}</span>
    </div>
  );
};

export default Game;
