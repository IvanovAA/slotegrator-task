import React from "react";
import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.Footer}>
      <nav className={styles.Navigation}>
        <ul className={styles.Links}>
          <li className={styles.NavPoint}>
            <a className={styles.NavLink} href="/">
              about casino
            </a>
          </li>
          <div className={styles.NavSeparator} />
          <li className={styles.NavPoint}>
            <a className={styles.NavLink} href="/">
              terms and conditions
            </a>
          </li>
          <div className={styles.NavSeparator} />
          <li className={styles.NavPoint}>
            <a className={styles.NavLink} href="/">
              responsible gaming
            </a>
          </li>
          <div className={styles.NavSeparator} />
          <li className={styles.NavPoint}>
            <a className={styles.NavLink} href="/">
              contact us
            </a>
          </li>
        </ul>
      </nav>
      <section className={styles.Payment}>
        <div className={styles.PaymentSeparator} />
        <div className={styles.PaymentItems}>
          <div className={styles.Mastercard} />
          <div className={styles.YandexMoney} />
          <div className={styles.Visa} />
          <div className={styles.Liqpay} />
          <div className={styles.Qiwi} />
        </div>
        <div className={styles.PaymentSeparator} />
      </section>
      <span className={styles.Copyright}>&copy;2015. all rights reserved.</span>
    </footer>
  );
};

export default Footer;
