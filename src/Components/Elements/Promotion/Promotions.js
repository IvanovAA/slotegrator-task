import React from "react";
import styles from "./Promotions.module.css";

const Promotions = () => {
  return (
    <div className={styles.Promotions}>
      <h4 className={styles.Header}>promotions</h4>
      <div className={styles.Slider}>
        <ul className={styles.Points}>
          <li className={styles.Point} />
          <li className={styles.Point} />
          <li className={styles.Point} />
          <li className={styles.Point} />
        </ul>
        <span className={styles.Offer} />
        <span className={styles.Description}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore.
        </span>
      </div>
    </div>
  );
};

export default Promotions;
