import React from "react";
import styles from "./HeaderNav.module.css";

const HeaderNav = () => {
  return (
    <nav className={styles.Nav}>
      <ul className={styles.List}>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            все игры
          </a>
        </li>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            пополнение счета
          </a>
        </li>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            получить выигрыш
          </a>
        </li>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            бонусы
          </a>
        </li>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            мобильная версия
          </a>
        </li>
        <li className={styles.Point}>
          <a href="/" className={styles.Link}>
            контакты
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderNav;
