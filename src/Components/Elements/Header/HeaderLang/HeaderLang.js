import React from "react";
import styles from "./HeaderLang.module.css";

const HeaderLang = props => {
  const cls = [styles.Country];

  if (props.selectedLang === "english") {
    cls.push(styles.Eng);
  } else if (props.selectedLang === "russian") {
    cls.push(styles.Rus);
  }

  return (
    <form className={styles.LangForm}>
      <div className={cls.join(" ")} />
      <select
        className={styles.Select}
        value={props.selectedLang}
        onChange={props.handleSelectChange}
      >
        <option value="english">English</option>
        <option value="russian">Русский</option>
      </select>
    </form>
  );
};

export default HeaderLang;
