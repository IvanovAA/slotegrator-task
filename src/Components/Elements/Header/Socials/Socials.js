import React from "react";
import styles from "./Socials.module.css";

const Socials = () => {
  const Facebook = styles.Button + " " + styles.Fb;
  const Twitter = styles.Button + " " + styles.Tw;
  const Vk = styles.Button + " " + styles.Vk;

  return (
    <div className={styles.Socials}>
      <button className={Facebook} />
      <button className={Twitter} />
      <button className={Vk} />
    </div>
  );
};

export default Socials;
