import React from "react";
import styles from "./MobileNav.module.css";

const MobileNav = props => {
  const cls = [styles.MobileLinks];

  if (props.isMenuOpen) {
    cls.push(styles.MobileLinksOpen);
  } else {
    cls.push(styles.MobileLinksClose);
  }

  return (
    <div className={styles.MobileNav}>
      <div className={styles.Toggle} onClick={props.toggleMenu}>
        <div className={styles.ToggleLine} />
        <div className={styles.ToggleLine} />
        <div className={styles.ToggleLine} />
      </div>
      <ul className={cls.join(" ")}>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            все игры
          </a>
        </li>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            пополнение счета
          </a>
        </li>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            получить выигрыш
          </a>
        </li>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            бонусы
          </a>
        </li>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            мобильная версия
          </a>
        </li>
        <li className={styles.MobilePoint}>
          <a className={styles.MobileLink} href="/">
            контакты
          </a>
        </li>
      </ul>
    </div>
  );
};

export default MobileNav;
