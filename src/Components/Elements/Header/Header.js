import React from "react";
import styles from "./Header.module.css";
import MobileNav from "./HeaderMobile/MobileNav";
import HeaderNav from "./HeaderNav/HeaderNav";
import HeaderLang from "./HeaderLang/HeaderLang";
import Socials from "./Socials/Socials";
import RegistrationButton from "./RegistrationButton/RegistrationButton";

const Header = props => {
  return (
    <header className={styles.Header}>
      <div className={styles.Container}>
        <MobileNav
          isMenuOpen={props.isMenuOpen}
          toggleMenu={props.toggleMenu}
        />
        <HeaderNav />
        <RegistrationButton />
        <Socials />
        <HeaderLang
          selectedLang={props.selectedLang}
          handleSelectChange={props.handleSelectChange}
        />
      </div>
    </header>
  );
};

export default Header;
