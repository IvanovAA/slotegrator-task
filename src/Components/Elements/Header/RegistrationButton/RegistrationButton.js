import React from "react";
import styles from "./RegistrationButton.module.css";

const RegistrationButton = () => {
  return <button className={styles.RegButton}>быстрая регистрация</button>;
};

export default RegistrationButton;
