import React from "react";
import styles from "./Login.module.css";
import Button from "../../Buttons/Button";

const Login = () => {
  return (
    <section className={styles.Login}>
      <div className={styles.Container}>
        <Button color="blue" className={styles.RegButton}>
          registration
        </Button>
        <form className={styles.Form}>
          <input type="text" placeholder="Username" className={styles.Input} />
          <div className={styles.PasswordContainer}>
            <input
              type="password"
              placeholder="Password"
              name="password"
              id="password"
              className={styles.Input}
            />
            <label htmlFor="password">
              <a className={styles.LabelLink} href="/">
                Forgot your password?
              </a>
            </label>
          </div>
          <Button color="red" className={styles.LoginButton}>
            login
          </Button>
        </form>
      </div>
    </section>
  );
};

export default Login;
