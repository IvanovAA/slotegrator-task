import React from "react";
import styles from "./PreviewGames.module.css";

import Game from "../Game/Game";

const PreviewGames = props => {
  return (
    <div className={styles.PreviewsGames}>
      {props.previewGames.map(item => {
        return (
          <Game
            className={styles.GameItem}
            key={item.key}
            url={item.url}
            title={item.title}
          />
        );
      })}
    </div>
  );
};

export default PreviewGames;
