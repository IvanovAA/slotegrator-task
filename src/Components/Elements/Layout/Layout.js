import React from "react";
import styles from "./Layout.module.css";
import logo from "../../../assets/images/slider-desktop.png";

const Layout = () => {
  return (
    <div className={styles.Layout}>
      <img src={logo} alt="Spin_&_Win" className={styles.Logo} />
    </div>
  );
};

export default Layout;
