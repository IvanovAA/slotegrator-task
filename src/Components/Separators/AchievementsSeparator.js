import React from "react";
import styles from "./AchievementsSeparator.module.css";

const AchievementsSeparator = () => {
  return <div className={styles.AchievementsLine} />;
};

export default AchievementsSeparator;
