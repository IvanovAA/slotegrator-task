import React from "react";
import styles from "./TopSeparator.module.css";

const TopSeparator = () => {
  return <div className={styles.TopLine} />;
};

export default TopSeparator;
