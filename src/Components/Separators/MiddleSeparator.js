import React from "react";
import styles from "./MiddleSeparator.module.css";

const MiddleSeparator = () => {
  return <div className={styles.MiddleLine} />;
};

export default MiddleSeparator;
