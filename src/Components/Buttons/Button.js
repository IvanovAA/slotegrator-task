import React from "react";
import styleBlue from "./BlueButton.module.css";
import styleRed from "./RedButton.module.css";

const Button = props => {
  const ImportedStyle = props.className;
  const BlueStyle = ImportedStyle + " " + styleBlue.BlueButton;
  const RedStyle = ImportedStyle + " " + styleRed.RedButton;
  if (props.color === "blue") {
    return (
      <button type="button" className={BlueStyle}>
        {props.children}
      </button>
    );
  } else if (props.color === "red") {
    return (
      <button type="button" className={RedStyle}>
        {props.children}
      </button>
    );
  }
};

export default Button;
