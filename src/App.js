import React from "react";
import Header from "./Components/Elements/Header/Header";
import TopSeparator from "./Components/Separators/TopSeparator";
import Login from "./Components/Elements/Login/Login";
import Layout from "./Components/Elements/Layout/Layout";
import MiddleSeparator from "./Components/Separators/MiddleSeparator";
import Main from "./Components/Elements/Main/Main";
import Footer from "./Components/Elements/Footer/Footer";

export default class App extends React.Component {
  state = {
    selectedLang: "english",
    isMenuOpen: false,
    winners: [],
    previewGames: [],
    allGames: []
  };

  componentDidMount() {
    fetch("http://localhost:3000/lastWinners")
      .then(response => response.json())
      .then(data =>
        this.setState({
          winners: data
        })
      );
    fetch("http://localhost:3000/previewGames")
      .then(response => response.json())
      .then(data =>
        this.setState({
          previewGames: data
        })
      );
    fetch("http://localhost:3000/allGames")
      .then(response => response.json())
      .then(data =>
        this.setState({
          allGames: data
        })
      );
  }

  toggleMenu = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };

  handleSelectChange = event => {
    const target = event.target;
    this.setState({ selectedLang: target.value });
  };

  render() {
    return (
      <>
        <Header
          selectedLang={this.state.selectedLang}
          handleSelectChange={this.handleSelectChange}
          toggleMenu={this.toggleMenu}
          isMenuOpen={this.state.isMenuOpen}
        />
        <TopSeparator />
        <Login />
        <TopSeparator />
        <Layout />
        <MiddleSeparator />
        <Main
          winners={this.state.winners}
          previewGames={this.state.previewGames}
          allGames={this.state.allGames}
        />
        <MiddleSeparator />
        <Footer />
      </>
    );
  }
}
