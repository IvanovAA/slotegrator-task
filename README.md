This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### task requirements

Make a layout of the main page, and due to the lack of a layout for mobile permissions show independence.
To work, use automation tools.

## Available Scripts

In the project directory, you can run:

### `npm install -g json-server` - install JSON Server
### `npm i`

### `json-server --watch db.json` in 1-st console

Runs local server (http://localhost:3000)

### `npm start` in 2-nd console

Runs the app in the development mode (http://localhost:3001)


